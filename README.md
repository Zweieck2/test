# FriedenSau Redaktion

Repository der FriedenSau. Hier werden Artikel geschrieben und Materialien gesammelt.
Alles ist versioniert und mit hilfreichen Projektmanagement-Werkzeugen verknüpft.

***(DEMO)***

## Arbeitsablauf

### Idee und Recherche

Am Anfang steht die Idee für einen Artikel.
Diese wird zunächst unter **Issues** eingetragen und vorgestellt (**Neues Issue**).
Der Titel sollte dabei kurz und prägnant den Kern der Idee beschreiben (z.B. "Neue Redaktion").
Dann sollte die Idee hinreichend beschrieben werden.
Das Issue sollte über die Seitenleiste mit dem Laben "**Artikel-Idee**" als solche gekennzeichnet werden.
Nach dem Erstellen kann sie so von dem ganzen Team gesehen werden.

### Artikel schreiben

In dem Issue können nun bereits konkrete Informationen und Ideen zur Ausgestaltung gesammelt werden.
Vielversprechende Issues werden dann dem **Meilenstein** einer konkreten Ausgabe zugeordnet oder nur statt "Artikel-Idee" als "**Writing in Progress**" gekennzeichnet.
Idealerweise wird auch gleich ein Mitarbeiter als **Zuständig** markiert.
Dieser kann nun beginnen, den Artikel auszuformulieren.

Dazu sollte im Verzeichnis `artikel` eine **Neue Datei** angelegt werden.
Um die Datei in einem eigenen Verzeichnis abzulegen, kann dessen Namen durch einen *Forward*-Slash (**`/`**) getrennt vor den Dateinamen geschrieben werden.
Der Verzeichnisname sollte möglichst kurz, in Kleinbuchstaben und mit Bindestrichen statt Leerzeichen sein (z.B. `nasser-fleck`).
Der Artikel, der letztendlich in der Ausgabe erscheinen wird, sollte immer **`README.md`** benannt werden.
So wird der Inhalt sofort angezeigt, wenn man das Verzeichnis betritt, und es gibt im Satz keine Unsicherheit, was der eigentliche Artikel sein soll.
In dem Verzeichnis können außerdem artikelspezifische Fotos hochgeladen oder beliebig in Hilfsdateien gearbeitet werden.

![image](/allgemeine-ressourcen/screenshots/gitea-artikelmd.png)

Währenddessen kann das Issue weiter für neue Informationen und zur Diskussion genutzt werden.
Mitarbeiter können mit vorangestelltem `@` erwähnt werden und bekommen das als Benachrichtigung in der oberen rechten Ecke angezeigt, sobald sie das nächste Mal eine beliebige Seite der Plattform aufrufen.

Änderungen müssen eingecheckt werden ("committen").
Um in der Versionsverwaltung den Überblick zu behalten, sollte die Bezeichnung den Inhalt der Änderung widerspiegeln.
Zuerst sollte der bearbeitete Artikel erwähnt werden (Name des Verzeichnisses) und dann die Änderung.
Das muss aber nicht zu detailliert geschehen, da es nur der groben Einordnung dient.\
Beispiele:
- nasser-fleck: Artikel angefangen
- nasser-fleck: Artikel weitergeschrieben
- nasser-fleck: Satzzeichen vergessen
- nasser-fleck: Artikel weitergeschrieben
- nasser-fleck: typos berichtigt
- nasser-fleck: Endparagraph
- nasser-fleck: Artikel umgeschrieben
- ...

### Korrekturlesen und Satz

Ist der Artikel aus Sicht des Autors fertiggeschrieben, kann er korrekturgelesen werden.
Das wird durch Hinzufügen des Labels "**Korrekturlesen**" zu dem zugehörigen Issue signalisiert.
Nun können diesen Mitarbeiter finden, die gerade frei sind oder selbst vom Schreiben eine Pause machen, und Fehler anmerken.
Werden keine Fehler gefunden, wird dies ebenfalls als Nachricht im Issue vermerkt.
Dabei muss jeweils der **Permalink** der Datei mit angegeben werden, um die genaue korrekturgelesene Version zu referenzieren.

![image](/allgemeine-ressourcen/screenshots/gitea-permalink.png)

Haben zwei Mitarbeiter den Artikel in der aktuellen Fassung korrekturgelesen, ohne Fehler festzustellen, kann das Label "Korrekturlesen" entfernt und das Issue als "**Druckfertig**" markiert werden.

Druckfertige Artikel werden schließlich in die Ausgabe eingearbeitet und die zugehörigen Issues geschlossen.
Fertige Ausgaben werden in `/ausgaben` versioniert.
