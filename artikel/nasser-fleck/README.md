## Überfall auf Peter N.

# Der nasse Fleck ist hat wieder zugeschlagen!

2017 berichteten wir bereits vom mysteriösen nassen Fleck.
Offensichtlich war das kein Einzelfall.

Blablabla bla
Nachdem wir eine Stunde nach Abfahrt unsere Heimatstadt verließen, schafften wir es endlich auf die Autobahn.
Nun kann uns nichts mehr aufhalten.
Na gut, fast nichts.
Außer den anderen Autofahrern eben.
Solche, die sich nicht an den anderen vorbei trauen.
Solche, die berg-auf durch waghalsige Überholmanöver mit 0,5 km/h mehr als der Nabenmann immer wieder mittelschwere Staus auslösen.
Und dann ist da noch dieser nasse Fleck.
Ich bekam nasse Füße – und das nicht, weil ich so aufgeregt vor dem PfiJu war.
Auch die Trinkflaschen waren dicht.
Ganz im Gegensatz zu uns.
Auf der Raststätte konnte nicht mal der Autofahrer und Fahrzeugtechnikstudent (ein gewisser Oli H., Anm.d.R.) eine Ursache finden.
Und nein ihr Nasen, das Kühlwasser ist es nicht.
Und ich habe auch keine schwitzigen Füße!

Wer sich auskennt (und das meinen wir ernsthaft!) oder einfach jemanden zum Quatschen braucht, darf sich gerne im Leipziger Lager mit dem Codewort „Ambiguitätstoleranz“ melden.
Bei Fremdwortphobie kann alternativ auch „Wellensittich“ genannt werden.
Wir hoffen auf fachkundige Hilfe!
